package hrytsenko.example;

import java.util.List;
import java.util.Map;

public final class Newsletters {

    private Newsletters() {
    }

    /**
     * Select list of topics for each subscriber.
     * 
     * @param newsletters
     *            the list of newsletters.
     * 
     * @return the topics for each subscriber.
     */
    public static Map<String, List<String>> findTopics(List<Newsletter> newsletters) {
        throw new UnsupportedOperationException();
    }

}
